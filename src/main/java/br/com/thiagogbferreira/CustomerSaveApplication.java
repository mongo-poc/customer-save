package br.com.thiagogbferreira;

import br.com.thiagogbferreira.mongodb.reactive.domain.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableMongoAuditing
@Slf4j
public class CustomerSaveApplication {
  @Autowired
  CustomerRepository customerRepository;


  public static void main(String[] args) {
    SpringApplication.run(CustomerSaveApplication.class, args);
  }

  @Bean
  RouterFunction<?> routerFunction(CustomerSaveApplication routerHandlers) {
    return RouterFunctions
        .route(
            RequestPredicates.POST("/customers"),
            routerHandlers::saveCustomer
        )
        ;
  }

  public Mono<ServerResponse> saveCustomer(ServerRequest serverRequest) {
    log.info("save customer");

    return ServerResponse.ok()
        .body(serverRequest.bodyToMono(Customer.class).flatMap(customerRepository::save), Customer.class);
  }
}

interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {
}
