FROM openjdk:8-jdk-alpine
LABEL version=1.0.0 \
      group=br.com.thiagogbferreira.mongodb.reactive \
      artifact=customer-save
VOLUME /tmp
COPY target/customer-save-1.0.0.jar app.jar
ENV JAVA_OPTS=""

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar

